from fastapi import FastAPI
# from pydantic import BaseModel
from typing import List

app = FastAPI()

# class CDMRequest(BaseModel):
#     satellite_id: str
#     timestamp: str
#     conjunction_data: List[float]

@app.post("/cdm")
async def handle_cdm_request(cdm_request):
    data = dict()
    data["req_cdm"] = cdm_request
    tca = 2
    data["tca"] = tca
    pc = 4
    data["pc"] = pc
    return data

# src/cdm_mockup_api
# pyproject.toml


